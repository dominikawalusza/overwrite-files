#SKRYPT NADPISUJĄCY PLIKI O TAKICH SAMYCH NAZWACH ALE Z INNYMI ROZSZERZENIAMI

#import niezbędnych bibliotek
import os
import shutil

#zmienne
dir_selected = r"C:\Users\dwa\Desktop\wodgik\grafika\wybrane" #ścieżka folderu z wybranymi plikami
dir_all = r"C:\Users\dwa\Desktop\wodgik\grafika\wszystkie" #ścieżka folderu ze wszystkimi plikami
extension_all = ".jpg" #rozszerzenie plików nadpisujących
extension_selected = ".png" #rozszerzenie plików do nadpisania

#tworzenie list z plikami z danych folderów
list_selected = os.listdir(dir_selected)
list_all = os.listdir(dir_all)

#tworzenie listy z plikami wybranymi bez rozszerzenia
list_selected_new = []
for i in list_selected:
    list_selected_new.append(i.split('.')[0])

#tworzenie listy ze wszystkimi plikami bez rozszerzenia
list_all_new = []
for i in list_all:
    list_all_new.append(i.split('.')[0])

#kopiowanie plików o tej samej nazwie z folderu ze wszsystkimi plikami do folderu z wybranymi plikami
for file in list_selected_new:
    if file in list_all_new:
        shutil.copy(dir_all + "/" + file + extension_all, dir_selected)

#usuwanie starych wybranych plików
for item in list_selected:
    if item.endswith(extension_selected ):
        os.remove(os.path.join(dir_selected, item))

print(list_selected) #lista plików końcowych w folderze z wybranymi plikami


